﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.SqlServer.Management.Common;
using Gooey;
using Microsoft.SqlServer.Management.Sdk.Sfc;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace SqlServerScripter {
	public partial class frmMain : Form {
		#region Private vars
		private Utilities _instanceUtils = null;
		private Utilities _utils {
			get {
				if (_instanceUtils == null) {
					_instanceUtils = new Utilities();
				}

				return _instanceUtils;
			}
		}
		private uint? _maxOutputFileSizeBytes = null;
		private SssConfig _config;
		#endregion

		#region Private methods
		private Server setupConnection() {
			ServerConnection conn = new ServerConnection();
			conn.ConnectionString = txtDbConnectionString.Text;
			return new Server(conn);
		}

		private void showExceptionError(string errorMsg, Exception ex) {
				string inner = null;
				if (ex.InnerException != null) {
					inner = ex.InnerException.Message;
				}
				_utils.ShowError(errorMsg + "\n     -> " + ex.Message + (ex.InnerException == null ? "" : "\n          -> " + ex.InnerException.Message));
		}

		private void enableDbSelector(bool isEnabled) {
			cbChooseDb.Items.Clear();
			lblChooseDb.Enabled = isEnabled;
			cbChooseDb.Enabled = isEnabled;
		}

		private void enableExportScript(bool isEnabled) {
			lblExportToFile.Enabled = isEnabled;
			txtExportToFile.Enabled = isEnabled;
			btnExportToFiles.Enabled = isEnabled;
			btnBrowse.Enabled = isEnabled;
		}

		private bool validatePathString(string pathStr, out string path, out string prefix, out string extension) {
			// We won't create any directories, but we obviously have to create files.  Therefore, if the path
			// points to a non-existent entity, we assume it should be a file, and the directory portion of the path
			// should indicate a directory that already exists into which we can write our files.

			path = null;
			prefix = null;
			extension = null;

			FileInfo infFile = null;
			try {
				infFile = new FileInfo(pathStr);
			}
			catch (ArgumentException) {}

			DirectoryInfo infDir = null;
			try {
				infDir = new DirectoryInfo(pathStr);
			}
			catch (ArgumentException) {}

			if (infDir != null && infDir.Exists) {
				// Existent directory
				path = infDir.FullName;

				if (infDir.ToString().TrimEnd(new char[] { '\\' }) != infDir.FullName.TrimEnd(new char[] { '\\' })) {
					// Non-absolute
					return false;
				}
			}
			else if (infFile != null && !string.IsNullOrEmpty(infFile.Name)) {
				// Maybe an existent file or existent directory?
				path = infFile.DirectoryName;
				extension = infFile.Extension;
				prefix = infFile.Name.TrimEnd(extension, false);
				extension = extension.TrimStart(new char[] { '.' });

				if (!infFile.Directory.Exists) {
					// Nope, non-existent file with non-existent directory
					return false;
				}
				else if (infFile.ToString() != infFile.FullName) {
					// Non-absolute
					return false;
				}
			}
			else {
				// Non-existent directory
				path = "[empty]";
				if (infDir != null) {
					path = infDir.FullName;
				}

				return false;
			}

			return true;
		}
		#endregion

		#region Constructors
		public frmMain(SssConfig config) {
			InitializeComponent();

			this._config = config;

			this.Text += " v" + _utils.GetVersionString(Assembly.GetExecutingAssembly(), VersionStringType.MajorMinor);
			txtDbConnectionString.Text = ConfigurationManager.ConnectionStrings["DefaultDatabase"].ToString();
			txtMaxFileSize.Text = config.MaxOutputFileSizeBytes.HasValue ? config.MaxOutputFileSizeBytes.Value.ToString() : "(unlimited)";

			enableDbSelector(false);
			enableExportScript(false);

			btnConnect.Tag = false;
		}
		#endregion

		private void frmMain_Load(object sender, EventArgs ea) {
			// DO NOT put code in here!  Its Exception handling is broken in Windows 7 64 bit and later.  See:
			// http://stackoverflow.com/questions/4933958/vs2010-does-not-show-unhandled-exception-message-in-a-winforms-application-on-a
			// http://blog.paulbetts.org/index.php/2010/07/20/the-case-of-the-disappearing-onload-exception-user-mode-callback-exceptions-in-x64/
			// Put init code in the constructor instead.
		}

		private void btnConnect_Click(object sender, EventArgs ea) {
			if (!(bool)(btnConnect.Tag)) {
				// Not connected
				Server srv = setupConnection();

				try {
					string srvVersion = srv.Information.Version.ToString();
					List<string> dbNames = new List<string>();
					foreach (Database db in srv.Databases) {
						dbNames.Add(db.Name);
					}
					_utils.ShowInfo("Connected to SQL Server; version: " + srvVersion);
					txtDbConnectionString.Enabled = false;
					enableDbSelector(true);
					btnConnect.Text = "Disconnect";
					btnConnect.Tag = true;

					foreach (string dbName in dbNames) {
						cbChooseDb.Items.Add(dbName);
					}
				}
				catch (Exception ex) {
					showExceptionError("Couldn't connect to SQL Server.", ex);
				}
			}
			else {
				// Connected
				txtDbConnectionString.Enabled = true;
				enableDbSelector(false);
				enableExportScript(false);
				txtExportToFile.Text = "";
				btnConnect.Text = "Connect";
				btnConnect.Tag = false;
			}
		}

		private void btnExportScript_Click(object sender, EventArgs ea) {
			Server srv = setupConnection();

			// Reference the database
			if (!srv.Databases.Contains(cbChooseDb.SelectedItem.ToString())) {
				_utils.ShowError(@"Couldn't find DB ""{0}"".".FormatWith(cbChooseDb.SelectedItem.ToString()));
				return;
			}
			Database db = srv.Databases[cbChooseDb.SelectedItem.ToString()];

			string path;
			string prefix;
			string extension;
			if (!validatePathString(txtExportToFile.Text, out path, out prefix, out extension)) {
				_utils.ShowError(@"Specified output path ""{0}"" doesn't exist, or is not absolute.".FormatWith(txtExportToFile.Text));
				return;
			}

			// Default to .sql extension
			if (string.IsNullOrEmpty(extension)) {
				extension = "sql";
			}

			using (var writ = new LimitedFileSizeWriter(this._config.MaxOutputFileSizeBytes, path, prefix, extension) { TypeOfNewline = LimitedFileSizeWriter.NewlineType.WindowsNotepad }) {
				try {
					Scripter scrp = new Scripter(srv);
					scrp.Options.IncludeHeaders = true;
					scrp.Options.AppendToFile = false;
					scrp.Options.ToFileOnly = false;
					scrp.Options.Indexes = true;                  // Include indexes
					scrp.Options.DriAll = true;                   // Include DRI objects in the script
					scrp.Options.Triggers = true;                 // Include triggers
					scrp.Options.FullTextIndexes = true;          // Include full text indexes
					scrp.Options.NonClusteredIndexes = true;      // Include non-clustered indexes
					scrp.Options.NoCollation = false;             // Include collation
					scrp.Options.Bindings = true;                 // Include bindings
					scrp.Options.SchemaQualify = true;            // Include schema qualification, eg. [dbo]
					scrp.Options.IncludeDatabaseContext = false;
					scrp.Options.FullTextStopLists = true;
					scrp.Options.IncludeIfNotExists = false;
					scrp.Options.ScriptBatchTerminator = true;
					scrp.Options.ExtendedProperties = true;
					scrp.Options.ClusteredIndexes = true;
					scrp.Options.FullTextCatalogs = true;
					scrp.Options.SchemaQualifyForeignKeysReferences = true;
					scrp.Options.XmlIndexes = true;

					// Both the schema and the data will be scripted.  The Drop statement for existing data will
					// not be scripted because the script generated will first create the table and then insert
					// the data.  Data does not exist in the tables to begin with.
					scrp.Options.ScriptSchema = true;
					scrp.Options.ScriptData = true;
					scrp.Options.ScriptDrops = false;

					// Prefectching may speed things up
					scrp.PrefetchObjects = true;

					var urns = new List<Urn>();

					// Iterate through the schemas in database and script each one.
					foreach (Schema sch in db.Schemas) {
						if (!sch.IsSystemObject) {
							urns.Add(sch.Urn);
						}
					}

					// Iterate through the XML schema collections in database and script each one.
					foreach (XmlSchemaCollection xsch in db.XmlSchemaCollections) {
						urns.Add(xsch.Urn);
					}

					// Iterate through the full text catalogs in database and script each one.
					foreach (FullTextCatalog cat in db.FullTextCatalogs) {
						urns.Add(cat.Urn);
					}

					// Iterate through the user-defined data types in database and script each one.
					foreach (UserDefinedDataType udt in db.UserDefinedDataTypes) {
						urns.Add(udt.Urn);
					}

					// Iterate through the user-defined types in database and script each one.
					foreach (UserDefinedType ut in db.UserDefinedTypes) {
						urns.Add(ut.Urn);
					}

					// Iterate through the user-defined aggegates in database and script each one.
					foreach (UserDefinedAggregate ua in db.UserDefinedAggregates) {
						urns.Add(ua.Urn);
					}

					// Iterate through the user-defined table types in database and script each one.
					foreach (UserDefinedTableType utt in db.UserDefinedTableTypes) {
						urns.Add(utt.Urn);
					}

					// Iterate through the user-defined functions in database and script each one.
					foreach (UserDefinedFunction uf in db.UserDefinedFunctions) {
						if (!uf.IsSystemObject) {
							// UDF is not a system object, so add it.
							urns.Add(uf.Urn);
						}
					}

					// Iterate through the tables in database and script each one.
					foreach (Table tb in db.Tables) {
						if (!tb.IsSystemObject) {
							// Table is not a system object, so add it.
							urns.Add(tb.Urn);
						}
					}

					// Iterate through the views in database and script each one.
					foreach (Microsoft.SqlServer.Management.Smo.View view in db.Views) {
						if (!view.IsSystemObject) {
							// View is not a system object, so add it.
							urns.Add(view.Urn);
						}
					}

					// Iterate through the stored procedures in database and script each one.
					foreach (StoredProcedure sp in db.StoredProcedures) {
						if (!sp.IsSystemObject) {
							// Procedure is not a system object, so add it.
							urns.Add(sp.Urn);
						}
					}

					// Start by manually adding DB context as header
					writ.AppendHeaderLine("USE [" + db.Name + "]");
					writ.AppendHeaderLine("GO");

					// It's recommended that ANSI padding always be on
					writ.AppendHeaderLine("SET ANSI_PADDING ON");
					writ.AppendHeaderLine("GO");

					// It seems each scriptChunk is a sensible batch, and putting GO after it makes it work in tools like SSMS.
					// Wrapping each scriptChunk in an 'exec' statement would work better if using SqlCommand to run the script.
					writ.AppendSuffixLine("GO");

					List<string> totalBuffer = new List<string>();
					List<String> contentBuffer = new List<string>();
					bool buffering = false;
					string strFinishBufferingPrefix = null;
					Regex reFinishBuffering = null;

					IEnumerable<string> scriptChunks = scrp.EnumScript(urns.ToArray());
					foreach (string scriptChunk in scriptChunks) {
						if (!buffering) {
							if (scriptChunk.StartsWith("SET IDENTITY_INSERT") && Regex.IsMatch(scriptChunk, @"SET IDENTITY_INSERT.*\bON\s*$")) {
								// Start buffering
								totalBuffer.Add(scriptChunk.Trim(new char[] { '\r', '\n' }));

								strFinishBufferingPrefix = "SET IDENTITY_INSERT";
								reFinishBuffering = new Regex(@"SET IDENTITY_INSERT.*\bOFF\s*$");
								buffering = true;
							}
							else {
								writ.WriteLine(scriptChunk.Trim(new char[] { '\r', '\n' }));
							}
						}
						else {
							if ((string.IsNullOrEmpty(strFinishBufferingPrefix) || scriptChunk.StartsWith(strFinishBufferingPrefix)) && (reFinishBuffering == null || reFinishBuffering.IsMatch(scriptChunk))) {
								// Finish buffering
								totalBuffer.Add(writ.CombineStringLines(contentBuffer));
								totalBuffer.Add(scriptChunk.Trim(new char[] { '\r', '\n' }));

								writ.Write(writ.CombineStringLines(totalBuffer));

								contentBuffer.Clear();
								totalBuffer.Clear();
								strFinishBufferingPrefix = null;
								reFinishBuffering = null;
								buffering = false;
							}
							else {
								// Continue buffering
								contentBuffer.Add(scriptChunk.Trim(new char[] { '\r', '\n' }));
							}
						}
					}

					// Flush buffer if still buffering
					if (buffering) {
						writ.Write(writ.CombineStringLines(totalBuffer));
					}
				}
				catch (Exception ex) {
					showExceptionError("Couldn't generate script, or write script to output file.", ex);
					return;
				}
			}

			_utils.ShowInfo(@"DB exported to script file(s) at: ""{0}\{1}<...>{2}""".FormatWith(path.TrimEnd(new char[] { '\\' }), prefix ?? "", string.IsNullOrEmpty(extension) ? "" : "." + extension));
		}

		private void btnBrowse_Click(object sender, EventArgs ea) {
			DialogResult result = dlgExportToFile.ShowDialog();

			switch (result) {
				case DialogResult.OK:
				case DialogResult.Yes:
					txtExportToFile.Text = dlgExportToFile.FileName;
					break;
			}
		}

		private void cbChooseDb_SelectedValueChanged(object sender, EventArgs e) {
			enableExportScript(true);
		}
	}
}
