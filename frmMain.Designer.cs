﻿namespace SqlServerScripter {
	partial class frmMain {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.lblHeading = new System.Windows.Forms.Label();
			this.btnConnect = new System.Windows.Forms.Button();
			this.txtDbConnectionString = new System.Windows.Forms.TextBox();
			this.lblDbConnectionString = new System.Windows.Forms.Label();
			this.btnExportToFiles = new System.Windows.Forms.Button();
			this.lblExportToFile = new System.Windows.Forms.Label();
			this.txtExportToFile = new System.Windows.Forms.TextBox();
			this.btnBrowse = new System.Windows.Forms.Button();
			this.dlgExportToFile = new System.Windows.Forms.SaveFileDialog();
			this.lblChooseDb = new System.Windows.Forms.Label();
			this.cbChooseDb = new System.Windows.Forms.ComboBox();
			this.lblExportToFileNote = new System.Windows.Forms.Label();
			this.lblMaxFileSize = new System.Windows.Forms.Label();
			this.txtMaxFileSize = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// lblHeading
			// 
			this.lblHeading.AutoSize = true;
			this.lblHeading.Location = new System.Drawing.Point(12, 9);
			this.lblHeading.Name = "lblHeading";
			this.lblHeading.Size = new System.Drawing.Size(444, 13);
			this.lblHeading.TabIndex = 10;
			this.lblHeading.Text = "Use SQL Server Management Objects (SMO) to convert a SQL Server database to a scr" +
    "ipt...";
			// 
			// btnConnect
			// 
			this.btnConnect.Location = new System.Drawing.Point(755, 63);
			this.btnConnect.Name = "btnConnect";
			this.btnConnect.Size = new System.Drawing.Size(117, 23);
			this.btnConnect.TabIndex = 22;
			this.btnConnect.Text = "Connect";
			this.btnConnect.UseVisualStyleBackColor = true;
			this.btnConnect.Click += new System.EventHandler(this.btnConnect_Click);
			// 
			// txtDbConnectionString
			// 
			this.txtDbConnectionString.Location = new System.Drawing.Point(15, 65);
			this.txtDbConnectionString.Name = "txtDbConnectionString";
			this.txtDbConnectionString.Size = new System.Drawing.Size(734, 20);
			this.txtDbConnectionString.TabIndex = 21;
			// 
			// lblDbConnectionString
			// 
			this.lblDbConnectionString.AutoSize = true;
			this.lblDbConnectionString.Location = new System.Drawing.Point(12, 49);
			this.lblDbConnectionString.Name = "lblDbConnectionString";
			this.lblDbConnectionString.Size = new System.Drawing.Size(435, 13);
			this.lblDbConnectionString.TabIndex = 20;
			this.lblDbConnectionString.Text = "Database connection string (initialized to config file\'s \"DefaultDatabase\" connec" +
    "tion string):";
			// 
			// btnExportToFiles
			// 
			this.btnExportToFiles.Location = new System.Drawing.Point(783, 206);
			this.btnExportToFiles.Name = "btnExportToFiles";
			this.btnExportToFiles.Size = new System.Drawing.Size(89, 23);
			this.btnExportToFiles.TabIndex = 53;
			this.btnExportToFiles.Text = "Export to file(s)";
			this.btnExportToFiles.UseVisualStyleBackColor = true;
			this.btnExportToFiles.Click += new System.EventHandler(this.btnExportScript_Click);
			// 
			// lblExportToFile
			// 
			this.lblExportToFile.AutoSize = true;
			this.lblExportToFile.Location = new System.Drawing.Point(12, 192);
			this.lblExportToFile.Name = "lblExportToFile";
			this.lblExportToFile.Size = new System.Drawing.Size(96, 13);
			this.lblExportToFile.TabIndex = 50;
			this.lblExportToFile.Text = "Export script to file:";
			// 
			// txtExportToFile
			// 
			this.txtExportToFile.Location = new System.Drawing.Point(15, 208);
			this.txtExportToFile.Name = "txtExportToFile";
			this.txtExportToFile.Size = new System.Drawing.Size(681, 20);
			this.txtExportToFile.TabIndex = 51;
			// 
			// btnBrowse
			// 
			this.btnBrowse.Location = new System.Drawing.Point(702, 206);
			this.btnBrowse.Name = "btnBrowse";
			this.btnBrowse.Size = new System.Drawing.Size(75, 23);
			this.btnBrowse.TabIndex = 52;
			this.btnBrowse.Text = "Browse...";
			this.btnBrowse.UseVisualStyleBackColor = true;
			this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
			// 
			// dlgExportToFile
			// 
			this.dlgExportToFile.DefaultExt = "sql";
			this.dlgExportToFile.Filter = "SQL Files|*.sql";
			this.dlgExportToFile.OverwritePrompt = false;
			this.dlgExportToFile.Title = "Choose SQL script file location";
			// 
			// lblChooseDb
			// 
			this.lblChooseDb.AutoSize = true;
			this.lblChooseDb.Location = new System.Drawing.Point(12, 95);
			this.lblChooseDb.Name = "lblChooseDb";
			this.lblChooseDb.Size = new System.Drawing.Size(137, 13);
			this.lblChooseDb.TabIndex = 30;
			this.lblChooseDb.Text = "Choose database to export:";
			// 
			// cbChooseDb
			// 
			this.cbChooseDb.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbChooseDb.FormattingEnabled = true;
			this.cbChooseDb.Location = new System.Drawing.Point(15, 111);
			this.cbChooseDb.Name = "cbChooseDb";
			this.cbChooseDb.Size = new System.Drawing.Size(734, 21);
			this.cbChooseDb.TabIndex = 31;
			this.cbChooseDb.SelectedValueChanged += new System.EventHandler(this.cbChooseDb_SelectedValueChanged);
			// 
			// lblExportToFileNote
			// 
			this.lblExportToFileNote.AutoSize = true;
			this.lblExportToFileNote.Location = new System.Drawing.Point(12, 231);
			this.lblExportToFileNote.Name = "lblExportToFileNote";
			this.lblExportToFileNote.Size = new System.Drawing.Size(548, 13);
			this.lblExportToFileNote.TabIndex = 54;
			this.lblExportToFileNote.Text = "(note: outputted files will be numbered incrementally, eg. C:\\dir\\1.sql 2.sql 3.s" +
    "ql or C:\\dir\\out1.xyz out2.xyz out3.xyz)";
			// 
			// lblMaxFileSize
			// 
			this.lblMaxFileSize.AutoSize = true;
			this.lblMaxFileSize.Location = new System.Drawing.Point(12, 146);
			this.lblMaxFileSize.Name = "lblMaxFileSize";
			this.lblMaxFileSize.Size = new System.Drawing.Size(403, 13);
			this.lblMaxFileSize.TabIndex = 40;
			this.lblMaxFileSize.Text = "Maximum output file size (fixed; set to config file\'s \"MaxOutputFileSizeBytes\" se" +
    "tting):";
			// 
			// txtMaxFileSize
			// 
			this.txtMaxFileSize.BackColor = System.Drawing.SystemColors.Info;
			this.txtMaxFileSize.Enabled = false;
			this.txtMaxFileSize.Location = new System.Drawing.Point(15, 162);
			this.txtMaxFileSize.Name = "txtMaxFileSize";
			this.txtMaxFileSize.ReadOnly = true;
			this.txtMaxFileSize.Size = new System.Drawing.Size(734, 20);
			this.txtMaxFileSize.TabIndex = 41;
			// 
			// frmMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(884, 255);
			this.Controls.Add(this.lblMaxFileSize);
			this.Controls.Add(this.txtMaxFileSize);
			this.Controls.Add(this.lblExportToFileNote);
			this.Controls.Add(this.cbChooseDb);
			this.Controls.Add(this.lblChooseDb);
			this.Controls.Add(this.btnBrowse);
			this.Controls.Add(this.txtExportToFile);
			this.Controls.Add(this.lblExportToFile);
			this.Controls.Add(this.btnExportToFiles);
			this.Controls.Add(this.lblDbConnectionString);
			this.Controls.Add(this.txtDbConnectionString);
			this.Controls.Add(this.btnConnect);
			this.Controls.Add(this.lblHeading);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "frmMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SQL Server Scripter";
			this.Load += new System.EventHandler(this.frmMain_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblHeading;
		private System.Windows.Forms.Button btnConnect;
		private System.Windows.Forms.TextBox txtDbConnectionString;
		private System.Windows.Forms.Label lblDbConnectionString;
		private System.Windows.Forms.Button btnExportToFiles;
		private System.Windows.Forms.Label lblExportToFile;
		private System.Windows.Forms.TextBox txtExportToFile;
		private System.Windows.Forms.Button btnBrowse;
		private System.Windows.Forms.SaveFileDialog dlgExportToFile;
		private System.Windows.Forms.Label lblChooseDb;
		private System.Windows.Forms.ComboBox cbChooseDb;
		private System.Windows.Forms.Label lblExportToFileNote;
		private System.Windows.Forms.Label lblMaxFileSize;
		private System.Windows.Forms.TextBox txtMaxFileSize;
	}
}

